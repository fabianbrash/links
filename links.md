## Uselful Links

[ReactTraining](https://courses.reacttraining.com/courses/enrolled/662878)

[JAMSTACK - Proxing](https://create-react-app.dev/docs/proxying-api-requests-in-development/)

[Issues - NodeJS](https://github.com/TypeStrong/ts-node/issues/922)

[Nginx SELinux](https://www.nginx.com/blog/using-nginx-plus-with-selinux/)

[Go - Gin](https://github.com/gin-gonic/examples/blob/master/http2/main.go)

[APIs in dev](https://create-react-app.dev/docs/proxying-api-requests-in-development/)

[Go Modules](https://medium.com/@fonseka.live/getting-started-with-go-modules-b3dac652066d)

[Go packages](https://golangcode.com/go-get-all-packages/)

[.dockerignore](https://codefresh.io/docker-tutorial/not-ignore-dockerignore-2/)

[Chakra UI - Example](https://blog.logrocket.com/how-to-create-forms-with-chakra-ui-in-react-apps/)

[ES6 console](https://es6console.com/kcp9jurm/)

[Desctructuring -Example](https://wesbos.com/destructuring-objects)

[Access props React](https://medium.com/@PhilipAndrews/react-how-to-access-props-in-a-functional-component-6bd4200b9e0b)

[Pass props React](https://www.robinwieruch.de/react-pass-props-to-component)

[certbot - plugins](https://certbot.eff.org/docs/using.html?highlight=dns)

[WSUS - config](https://www.prajwaldesai.com/install-configure-wsus-on-windows-server-2019/)

[WSUS- continued](https://docs.microsoft.com/en-us/answers/questions/49782/wsus-updates-on-client-but-no-installation.html)

[certbot - dns plugin](https://certbot-dns-route53.readthedocs.io/en/stable/)

[certbot - route53 plugin](https://certbot-dns-route53.readthedocs.io/en/stable/)

[useReducer](https://hswolff.com/blog/why-i-love-usereducer/)

[Material-Table](https://material-table.com/#/docs/install)

[Here’s How You Can Use Docker With React](https://medium.com/better-programming/heres-how-you-can-use-docker-with-create-react-app-3ee3a972b04e)

[Dockerizing a React App](https://mherman.org/blog/dockerizing-a-react-app/)

[Deckhouse](https://deckhouse.io/en/)

[aws s3 cli ref](https://docs.aws.amazon.com/cli/latest/reference/s3/sync.html)

[aws docs s3 sync](https://docs.aws.amazon.com/cli/latest/userguide/cli-services-s3-commands.html#using-s3-commands-managing-objects-sync)

[When to useMemo and useCallback](https://kentcdodds.com/blog/usememo-and-usecallback)

[Gist - build object from form values](https://gist.github.com/prof3ssorSt3v3/52ebd432bb7b8a155985a2f82509541d)

[Use ternaries rather than &&](https://kentcdodds.com/blog/use-ternaries-rather-than-and-and-in-jsx)

[Github issue - turn off redirects](https://github.com/nginxinc/docker-nginx-unprivileged/issues/60)

[Cilium - Network Policy](https://editor.cilium.io/?id=Sl9AzxtgBtxRanQ6)

[ngrok](https://dashboard.ngrok.com/login)

[Stackoverflow - Nginx — static file serving confusion with root & alias](https://stackoverflow.com/questions/10631933/nginx-static-file-serving-confusion-with-root-alias)

[Tanzu Basic - Building TKG Cluster](https://vmtechie.blog/2020/12/09/tanzu-basic-building-tkg-cluster/)

[Virtual Machine Classes for Tanzu Kubernetes Clusters](https://docs.vmware.com/en/VMware-vSphere/7.0/vmware-vsphere-with-tanzu/GUID-7351EEFF-4EF0-468F-A19B-6CEA40983D3D.html)

[VSPHERE WITH TANZU VCENTER SERVER NETWORK CONFIGURATION OVERVIEW](https://frankdenneman.nl/2020/11/06/vsphere-with-tanzu-vcenter-server-network-configuration-overview/)


[Makefile tutorial](https://makefiletutorial.com/)

[Linux - wc examples](https://cmdlinetips.com/2011/08/how-to-count-the-number-of-lines-words-and-characters-in-a-text-file-from-terminal/)

[CSS - flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

[20 best ui logins](https://www.mockplus.com/blog/post/login-page-examples)

[awesome chakra-ui](https://github.com/chakra-ui/awesome-chakra-ui)

[chakra-ui templates](https://chakra-templates.dev/)

[36 most beautiful css forms](https://uicookies.com/beautiful-css-forms/)

[npkill](https://www.npmjs.com/package/npkill)

[DiceBear Avatars](https://avatars.dicebear.com/)

[RapidAPI](https://rapidapi.com)

[Napkin.io](https://www.napkin.io/)

[KodeKloud - CKA notes](https://github.com/kodekloudhub/certified-kubernetes-administrator-course)

[KodeKloud - Kubernetes-the-hard-way](https://github.com/mmumshad/kubernetes-the-hard-way)

[Rally - DeFI](https://rally.io/)
